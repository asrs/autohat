# autohat

0. Prequisite
Use a fedora minimal iso installation with basic xserver *or*
Use a fedora server with standard

1. Quick automatic setup
```
wget https://gitlab.com/asrs/autohat/-/raw/master/setup.sh
sh ./setup.sh
```

2. Manual Setup
```
sudo dnf install -y git ansible

ansible-pull -U 'https://gitlab.com/asrs/autohat' -K ansible/main.yml

# if you want to reboot at the end
ansible-pull -U 'https://gitlab.com/asrs/autohat' -K ansible/main.yml -t all,reboot
```

```

# What it does ?

It will setup a Openbox Rice with: 
- Polybar
- Dmenu2
- Sakura Terminal
- PCmanFM
- Zathustra
- Mate-screenshot
- Mate-power
- Lxrand
- LxApperance
- brave Browser
- Vlc
- custom Zsh
- custom tmux
- custom Nvim

This rice consumed ~400mb at fresh start

# HotKeys

launcher
---

Win + space			// dmenu
Win + shift + space	// dmenu tool
Win + x				// dmenu power
Win + z				// dmenu switcher

app
---

Win + enter			// terminal
Win + shift + enter	// FileManager
Win + shift + p		// Mate-Screenshot
Win + shift + q		// kill window

Media
---

Win + m				// muted - unmuted
Win + ,				// sound down
Win + .				// sound up
Win + shift + ,		// dim down
Win + shift + .		// dim up

windows 
---

Win + h				// tile left
Win + j				// mini
Win + k				// maxi
Win + l				// tile right
Win + d				// toggle show desktop

Workspace 
---
Win + 1..7			// go to wk x
Win + shift + 1..7	// move wk to x
Win + o				// go to wk left
Win + p				// go to wk right

