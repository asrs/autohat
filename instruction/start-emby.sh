#!/bin/bash

mkdir -p emby-conf

podman run --rm \
  --name=emby \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Europe/London \
  -p 8096:8096 \
  -p 8920:8920 `#optional` \
  -v emby-conf:/config:z \
  -v "/home/mnhdrn/Videos:/data/movies:z" \
  ghcr.io/linuxserver/emby

