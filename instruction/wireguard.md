install wireguard-tools on both client & server

---
# Server

wg genkey | tee privatekey | wg pubkey > publickey

vim /etc/wireguard/wg0.conf
```
[Interface]
PrivateKey=<server-private-key> #copythetoken
Address=<server-ip-address>/<subnet>
SaveConfig=true
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o <public-interface> -j MASQUERADE;
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o <public-interface> -j MASQUERADE;
ListenPort = 51820
```
/!\ dont forget to change the public interface, ex: eth0

wg-quick up wg0

---
# Client

wg genkey | tee privatekey | wg pubkey > publickey

vim /etc/wireguard/wg0.conf
```
[Interface]
PrivateKey = <client-private-key>
Address = <client-ip-address>/<subnet>
SaveConfig = true

[Peer]
PublicKey = <server-public-key>
Endpoint = <server-public-ip-address>:51820
AllowedIPs = 0.0.0.0/0
PersistentKeepalive=30
```

---
# Add the client into the server

wg set wg0 peer <client-public-key> allowed-ips <client-ip-address>/32

/!\ the given ip address is the one in the tunnel, ex: 10.0.0.2/8

cat /proc/sys/net/ipv4/ip_forward
# if == 0 then :
sysctl -w net.ipv4.ip_forward=1

---

# On client 
wg-quick up/down wg0 (or name of the config on the client)

---
to import on nmcli just use the graphical user interface `nm-connection-editor`
to manually create the peer-config OR use the following command:
`nmcli con import type wireguard file /path/to/wireguard.conf`
