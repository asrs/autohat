sudo dnf install virt-viewer

then just run in it from dmenu or terminal

terminal is : virt-viewer

if you got an error that he doesn't connect run

    virt-viewer -c qemu:///system


---
[3:59 PM] Mehrdad Tahernia

https://github.com/vagrant-libvirt/vagrant-libvirt/blob/main/docs/configuration.markdown

vagrant-libvirt/configuration.markdown at main · vagrant-libvirt/vagrant-libvirt

Vagrant provider for libvirt. Contribute to vagrant-libvirt/vagrant-libvirt development by creating an account on GitHub.

[4:00 PM] Mehrdad Tahernia
https://www.rubydoc.info/gems/vagrant-libvirt/0.0.28
  File: README
  
    — Documentation for vagrant-libvirt (0.0.28)

virt-viewer --connect=qemu+ssh://clement@192.168.3.62/system build_host_build_host

  config.vm.provider :libvirt do |libvirt|
    # libvirt.host = "example.com"
    libvirt.cpu_mode = 'host-passthrough'
    libvirt.cpus = 4
    libvirt.memory = 2048
    libvirt.cpuset = '1-4,^3,6'
    libvirt.cputopology :sockets => '2', :cores => '2', :threads => '1'
    libvirt.graphics_type = 'spice'
    libvirt.video_type = 'qxl'
    libvirt.graphics_autoport = 'yes'
    libvirt.graphics_ip = "127.0.0.1"
  end

 



