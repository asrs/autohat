#### 1. `cd` into `assets/` and install the following NPM packages:
```bash
npm i react react-dom -S
npm i @babel/preset-react -D
npm i @babel/preset-typescript -S
npm install --save-dev @babel/plugin-proposal-class-properties
npm audit fix
```
#### 2. add the following line to `assets/.babelrc` file:
```javascript
{
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react" // <--- this line
    ],
    plugins: [
        "@babel/plugin-proposal-class-properties"
    ]
}
```

#### 3. test changes:

edit `lib/app_web/templates/page/index.html`
```html
<div id="root">
</div>
```
edit `assets/js/app.js`
```javascript
import React from "react";
import ReactDOM from "react-dom";

const Index = () => {
  return <div>Hello React!</div>;
};

ReactDOM.render(<Index />, document.getElementById("root"));
```

