-----------------------
# prepare machines

create 1 Lb machine ahead
create 3 machines as master node

# note on alpine conf for k3s

apk add curl vim git
swapoff -a
sudo vi /etc/fstab
rc-update add cgroups default

# Add Master Node

on node 1 : 
curl -sfL https://get.k3s.io | sh -s - server --cluster-init
curl -sfL https://get.k3s.io | K3S_TOKEN=SECRET INSTALL_K3S_EXEC="--write-kubeconfig-mode 644" sh -s - server --cluster-init

on the 2 other node :
cat /var/lib/rancher/server/node-token #on source node
curl -sfL https://get.k3s.io | K3S_TOKEN=SECRET sh -s - server --server https://<ip or hostname of server1>:6443


kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null

# Add Worker Node
curl -sfL https://get.k3s.io | K3S_URL=https://10.12.1.40:6443 K3S_TOKEN="TOKEN" sh -

kubectl label node cb2.4xyz.couchbase.com node-role.kubernetes.io/worker=worker

# Remote Control

copy the value of /etc/rancher/k3s/k3s.yaml to your machine
edit the file, to change the server url value

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

export KUBECONFIG=k3s.yaml
kubectl get pods --all-namespaces

