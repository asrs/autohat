export ZSH=$HOME/.ocha-zsh
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export KEYTIMEOUT=1
export EDITOR=nvim

#export GDK_DPI_SCALE=1
#export GTK_SCALE=2
export QT_AUTO_SCREEN_SCALE_FACTOR=0
#export QT_SCREEN_SCALE_FACTORS=1

source $ZSH/1-option.zsh
source $ZSH/2-alias.zsh
source $ZSH/3-syntax.zsh
source $ZSH/4-git.zsh
source $ZSH/5-vim.zsh
source $ZSH/6-prompt.zsh

export PATH="$PATH:$HOME/.scripts:$HOME/.local/bin"

# Add RVM CONFIG
#source $HOME/.rvm/scripts/rvm
#export PATH="$PATH:$HOME/.rvm/bin"

#export GEM_HOME=$HOME/.gem
#export PATH="$GEM_HOME/ruby/2.5.0/bin:$PATH"
#
# Add OCAML CONFIG
#eval $(opam env)
alias oc=kubectl
alias ocname='kubectl config set-context --current'

[ -n "$SSH_CONNECTION" ] && unset SSH_ASKPASS
export GIT_ASKPASS=
if [ "$TMUX" = "" ]; then tmux; fi


# if git make a repo slow add
	# git config --add oh-my-zsh.hide-dirty 1
	#

### man fc !!!
